---
layout: default
title: Home
---

# Terms & Conditions

The Source of Tales app and manaserv, the server software it connects to, are
free software; you can redistribute them and/or modify them under the terms of
the GNU General Public License as published by the Free Software Foundation;
either version 2 of the License, or (at your option) any later version.

Source of Tales is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE. See the [GNU General Public
License](https://www.gnu.org/licenses/old-licenses/gpl-2.0.html) for more
details.

The Source of Tales app and the server(s) is connects to store and process
personal data that you may provide to us, to provide our Service. It’s your
responsibility to keep your phone and access to the app secure.

If you’re using the app outside of an area with Wi-Fi, you should remember
that the terms of the agreement with your mobile network provider will still
apply. As a result, you may be charged by your mobile provider for the cost of
data for the duration of the connection while accessing the app, or other
third-party charges. In using the app, you’re accepting responsibility for any
such charges, including roaming data charges if you use the app outside of
your home territory (i.e. region or country) without turning off data roaming.
If you are not the bill payer for the device on which you’re using the app,
please be aware that we assume that you have received permission from the bill
payer for using the app.

At some point, we may wish to update the app. The requirements for the system
may change, and you’ll need to download the updates if you want to keep using
the app. Source of Tales Team does not promise that it will always update the
app so that it is relevant to you and/or works with the system version that
you have installed on your device. We may also wish to stop providing the app,
and may terminate use of it at any time without giving notice of termination
to you.

## Changes to This Terms and Conditions

We may update our Terms and Conditions from time to time. Thus, you are
advised to review this page periodically for any changes. We will notify you
of any changes by posting the new Terms and Conditions on this page.

These terms and conditions are effective as of 2023-05-24.

## Contact Us

If you have any questions or suggestions about our Terms and Conditions, do
not hesitate to contact us at admin@sourceoftales.org.

This Terms and Conditions page was written with the help of [App Privacy
Policy Generator](https://app-privacy-policy-generator.nisrulz.com/).
