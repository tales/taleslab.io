---
layout: default
title: Home
---

# Account Deletion

While manaserv does implement account deletion, it is currently not implemented
yet in the client user interface. If you would like to delete your account and
all associated data, please email admin@sourceoftales.org.
