---
layout: post
title: Early Access and Stellar Login!
author: Thorbjørn Lindeijer
---

An initial early access release of Source of Tales is [now available](https://thorbjorn.itch.io/source-of-tales) for Windows, Linux and Android! At the same time, it is now possible to log into the game using a Stellar Wallet ([Albedo](https://albedo.link/)) instead of using a username + password.

![The login screen, featuring a “Login with Stellar” button](/images/2023-05-30_login-with-stellar.png)

## Source of Tales is Back

The revival of this project was triggered by a grant from the [Stellar Development Foundation](https://www.stellar.org/foundation) as part of the [Stellar Community Fund #10](https://communityfund.stellar.org/projects/source-of-tales). The goal of this project was to integrate Stellar Blockchain as a way to login into the game as well as to use it to store character data. You can read more details about the initial plan and the benefits this could bring at [Tales on Stellar](https://sourceoftales.org/stellar/).

Unfortunately, I have so far only managed to implement Stellar-based login as an alternative to using a username + password. Reviving this project after 10 years took way more time than I had anticipated and so did working towards the first [official release](https://thorbjorn.itch.io/source-of-tales).

But the result is a persistent online game that can be easily tried out on most major platforms in early-access form (hopefully we can cover macOS and possibly iOS soon as well). The project welcomes new contributors, who have the chance to see their improvements go live almost immediately!

Regarding the SCF #10 grant, we think the additions to enable Stellar for login will provide a good base for further integration of the Stellar Blockchain and are looking forward to persueing this in the near future.

## Implementing Stellar Login

Since we didn’t want to be in a position to manage security sensitive data in the server or client, we’ve chosen to implement Stellar login based on external wallets. The most common Stellar wallets are browser-based and the first one we chose to integrate is the [Albedo](https://albedo.link/) wallet. Albedo stood out as the simplest option for the user, since it does not require installing anything, not even a browser extension.

### Introducing the Stellar Bridge

Our client is written in Qt/C++ and the manaserv server is also written in C++, so it was not immediately obvious how to interact with a browser wallet. We’ve chosen to introduce a NodeJS based  server, the [Mana Stellar Bridge](https://gitlab.com/manasource/mana-stellar-bridge). The [Stellar Login page](https://sourceoftales.org/stellar-login/) sends the login result to this bridge, which the bridge will verify before forwarding it to manaserv.

The remaining problem is that manaserv needs to know which of its connected clients authenticated with a particular Stellar public key. For this, we’ve introduced a randomly generated token. manaserv generates this token when a client connects and sends it to the client. The client opens the [Stellar Login page](https://sourceoftales.org/stellar-login/) with the token as a parameter. After calling the [Albedo public key API](https://albedo.link/playground?section=public_key), the public key, token and signature are sent to the Stellar Bridge, and from there to manaserv. In the end, manaserv can identify the account by the Stellar public key and the client using the token, and the login succeeds.

### Changes to manaserv

Two new dependencies were [added to manaserv](https://gitlab.com/manasource/manaserv/-/commit/ee4624e7904f37c411fc7215d7c41d7a1b6b7d4f) for communication with the Stellar Bridge: [uWebSockets](https://github.com/uNetworking/uWebSockets) for the connection and [RapidJSON](https://rapidjson.org/) for parsing the message with the token.

Previously manaserv required a user to have a unique email address. When a user uses Stellar login, their email remains empty, which required [a database change](https://gitlab.com/manasource/manaserv/-/commit/1f7c7fa192907d90a0ef6bbfbfe770a1e2fffa08) that allows the email field to be NULL.

### Changes to the Website

The Stellar login page [was added](https://gitlab.com/tales/tales.gitlab.io/-/commit/16e2afced4c3a31eef8ac3c296ee7ede13a4a315). I’ve also added a [Privacy Policy](https://sourceoftales.org/privacy) and a page about [account deletion](https://sourceoftales.org/account-deletion), as required for publishing the client on Google Play.

### Changes to the Client

A “Login with Stellar” button [was added](https://gitlab.com/tales/tales-client/-/commit/70f031014a2bd60ac1bdedd2b6b64153b60a71dc) that would request the login URL Added Stellar Login button that opens URL in browser.

To make the flow a little more user-friendly, I’ve also [added a page](https://gitlab.com/tales/tales-client/-/commit/2ffed29c47a4fc61e8a48b0721fd60bf0567e892) which the client shows while waiting for the user to authenticate themselves in the browser.

### Further Stellar Integration

This is where I could take the Stellar integration with the funds we requested. Unfortunately I vastly underestimated the effort, but I think we’re on a good track with the introduction of the Mana Stellar Bridge. As a NodeJS server connected to manaserv, the bridge is the perfect place to build out the interaction with the Stellar Blockchain based on the official [JS Stellar SDK](https://github.com/stellar/js-stellar-sdk).

## Contributors Welcome

If this project sparks your interest, you’re welcome to join our team. Whether it is the Stellar integration or the game in general, please [join the development](https://sourceoftales.org/development.html) and contribute to the code, the content, or even the design. We’d love to hear from you!
