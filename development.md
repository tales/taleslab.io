---
layout: default
title: Development
---

# Join the Development

Join us at **#sourceoftales** using [IRC](ircs://irc.freegamedev.net:6697/#sourceoftales), [Web](https://irc.freegamedev.net/kiwiirc/#sourceoftales), [XMPP](https://xmpp.f-hub.org/##sourceoftales@irc.f-hub.org?join) or [Matrix](https://matrix.f-hub.org/#/#irc_#sourceoftales:matrix.f-hub.org) to chat or discuss.

If you want to start directly producing new game content, create a fork and
see [How to setup a local Server](https://gitlab.com/tales/sourceoftales/wikis/How%20to%20setup%20a%20local%20server) to start developing.

It is recommended to get in contact first rather than creating content first.

Checkout <a href="https://gitlab.com/tales">GitLab</a> for our repositories.
