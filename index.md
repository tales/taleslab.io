---
layout: default
title: Home
---

# Source of Tales

This game started as an entry for the [Liberated Pixel Cup
2012](https://lpc.opengameart.org), where it was called 'Lurking Patrol
Comrades'. It is a massive multiplayer online roleplaying game.

<div class="text-center">
  <img id="screenshot" alt="">
  <script type="text/javascript">
document.getElementById("screenshot").src=
"{{ site.basepath }}/screenshots/tales"+Math.floor(Math.random()*3 + 1)+".png";
  </script>
</div>

## Starting the game

The game can be played by downloading it from
[itch.io](https://thorbjorn.itch.io/source-of-tales) or
[Google Play](https://play.google.com/store/apps/details?id=org.sourceoftales.client).
On desktop it is recommended to install the game using the [itch.io app](https://itch.io/app) since it will
automatically keep the game up to date.

<p class="text-center">
<iframe src="https://itch.io/embed/2072790?linkback=true&amp;border_width=0&amp;bg_color=eeeeee&amp;fg_color=222222&amp;link_color=C7531A" width="550" height="165" frameborder="0" style="border-radius: 10px; border: 2px solid #8f5902; background-color: #eeeeee;"><a href="https://thorbjorn.itch.io/source-of-tales">Source of Tales on itch.io</a></iframe>
</p>

If no releases are available yet for your platform, you could compile the client
from source. The client is written in C++, based on Qt and is easy to compile on
any operating system and can even be compiled for Android. See the
[development](development.html) page for more information.

## Latest News

<ul>
  {% for post in site.categories.news :limit 3 %}

  <li><span>{{ post.date | date_to_string }}</span> &raquo;
  <a href="{{ post.url }}">{{ post.title }}</a></li>

  {% endfor %}
</ul>
